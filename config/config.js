
const config = {
  app: {
    baseUrl: process.env.BASE_URL || 'http://localhost',
    port: process.env.PORT || 3000,
    getPath: (path) => `${config.app.baseUrl}:${config.app.port}${path}`
  },
  requestCodes: {
    OK: 200,
    BAD_REQUEST: 400,
    NOT_AUTHORIZED: 401,
    NOT_FOUND: 404
  }
};

module.exports = config;
