# AWS - ECR Example

An node application example to demonstrate how to use AWS-ECR registry

## Setup

The first step is setup an AWS policy to allow a specific user to manage the ECR, for that add the following policy to the user

```javascript
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecr:PutLifecyclePolicy",
                "ecr:CreateRepository",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetAuthorizationToken",
                "ecr:UploadLayerPart",
                "ecr:ListImages",
                "ecr:PutImage",
                "ecr:SetRepositoryPolicy",
                "ecr:BatchGetImage",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeImages",
                "ecr:DescribeRepositories",
                "ecr:InitiateLayerUpload",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetRepositoryPolicy"
            ],
            "Resource": "*"
        }
    ]
}
```

After that, install and configure AWS CLI for the user

* [installing AWS-CLI](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)

* [configuring AWS-CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)

# Usage

* Creating the repository

``` bash
# aws ecr create-repository --repository-name {namespace}/{repository-name}
aws ecr create-repository --repository-name mynamespace/aws-ecr-example
```


If everything succeed, the following message is received

``` javascript
{
    "repository": {
        "repositoryArn": "arn:aws:ecr:us-east-1:132412412346XX:repository/mynamespace/aws-ecr-example",
        "registryId": "132412412346XX",
        "repositoryName": "mynamespace/aws-ecr-example",
        "repositoryUri": "132412412346XX.dkr.ecr.us-east-1.amazonaws.com/mynamespace/aws-ecr-example",
        "createdAt": 1542627538.0
    }
}

```

In this message, the most important property is **repositoryUri**, which is the ECR`s address used to push the docker image

* Getting AWS Docker authorization token
``` bash
$(aws ecr get-login --no-include-email --region us-east-1)
```

* Building images with tags
``` bash
# docker build -t {ECR-URI}/{namespace}/{repository-name}:{tag} {path-to-dockerfile}
docker build -t 132412412346XX.dkr.ecr.us-east-1.amazonaws.com/mynamespace/aws-ecr-example:latest .
```

* Pushing Docker image to ECR
``` bash
# docker push {ECR-URI}/{namespace}/{repository-name}:{tag}
docker push 132412412346XX.dkr.ecr.us-east-1.amazonaws.com/mynamespace/aws-ecr-example:latest
```

* Pulling Docker image from ECR
``` bash
# docker container run {ECR-URI}/{namespace}/{repository-name}:{tag}
docker container run --rm -p 3000:3000 132412412346XX.dkr.ecr.us-east-1.amazonaws.com/mynamespace/aws-ecr-example:latest
```
This specific image runs on port 3000, that`s why the argument **-p 3000:3000** is used

* Testing

To check if everything is working open your browser and enter the url __**http://localhost:3000/health**__, the following JSON must be returned

``` javascript
{
    "status":"OK"
}
```

# ECR Lifecycle Policy

Lifecycle policy allows to save money based on storage, removing the last versions/tags of your repository

Ex:

* Remove untagged images older than 5 days

```javascript
{
      "rulePriority": 1,
      "description": "Expire images older than 5 days",
      "selection": {
        "tagStatus": "untagged",
        "countType": "sinceImagePushed",
        "countUnit": "days",
        "countNumber": 5
      },
      "action": {
        "type": "expire"
      }
    }
```

* Keep just the last 5 images from a specific tag prefix

``` javascript
{
      "rulePriority": 10,
      "description": "Keep just the 5 most recent images from tags: latest and prd-",
      "selection": {
        "tagStatus": "tagged",
        "tagPrefixList": [
          "latest",
          "prd-"
        ],
        "countType": "imageCountMoreThan",
        "countNumber": 5
      },
      "action": {
        "type": "expire"
      }
    }
```

Applying policies:

``` bash
aws ecr put-lifecycle-policy --repository-name "mynamespace/aws-ecr-example" --lifecycle-policy-text '{"rules":[{"rulePriority":1,"description":"Expire images older than 5 days","selection":{"tagStatus":"untagged","countType":"sinceImagePushed","countUnit":"days","countNumber":5},"action":{"type":"expire"}},{"rulePriority":10,"description":"Keep just the 5 most recent images from tags: latest and prd-","selection":{"tagStatus":"tagged","countType":"imageCountMoreThan","tagPrefixList":["latest","prd-"],"countNumber":5},"action":{"type":"expire"}}]}'
```

### That`s it Happy Coding ! 